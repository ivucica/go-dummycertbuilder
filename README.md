DummyCertBuilder
===

This package provides a way to build a dummy self-signed TLS certificate. It is
intentionally **providing insecure certificates**.

It's based on `crypto/tls/generate_cert.go`, which you should refer to for a
better example on how to do this.

Documentation: <http://godoc.org/badc0de.net/pkg/dummycertbuilder>
