// Modifications Copyright 2016 Ivan Vucica and licensed under a BSD license.
// Original Copyright 2009 The Go Authors and licensed under a BSD license.
// Original: crypto/tls/generate_cert.go

// This package provides a way to build a dummy TLS certificate. It currently
// lacks any sort of error checking. It is intentionally producing insecure
// certificates, for example by (where possible) using a constant stream of
// bytes instead of a cryptographically secure random number generator (e.g. in
// crypto/rand). An even better implementation would not generate a certificate
// at all, but would simply use constant certificate.
//
// In future, implementation of this package might be updated to do just that.
//
// Don't use it for anything except possibly tests.
//
// This is based on crypto/tls/generate_cert.go.

package dummycertbuilder // import "badc0de.net/pkg/dummycertbuilder"

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"io"
	"math/big"
	"net"
	"time"
)

// Return a public key for the provided private key.
func publicKey(priv interface{}) interface{} {
	switch k := priv.(type) {
	case *rsa.PrivateKey:
		return &k.PublicKey
	default:
		return nil
	}
}

// Return a pem block for a given private key.
func pemBlockForKey(priv interface{}) *pem.Block {
	switch k := priv.(type) {
	case *rsa.PrivateKey:
		return &pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(k)}
	default:
		return nil
	}
}

// Provides a stream of bytes which is constant and predictable.
//
// Currently, bytes linearly increasing by 1 in range 0-min(len(p), 255).
type ConstantReader struct{}

func (ConstantReader) Read(p []byte) (n int, err error) {
	if len(p) > 0 {
		cnt := 0
		for i := 0; i < len(p) && i < 256; i++ {
			p[i] = byte(i)
			cnt++
		}
		return cnt, nil
	}
	return 0, nil
}

// Returns a dummy certificate. Future updates might provide a constant dummy certificate instead.
func GetDummyCert() tls.Certificate {
	return BuildDummyCert()
}

// Returns a new dummy certificate.
func BuildDummyCert() tls.Certificate {
	var rsaBits int
	var cryptoRand io.Reader

	// Cryptographically INsecure generator.
	cryptoRand = &ConstantReader{}
	rsaBits = 128

	privateKey, _ := rsa.GenerateKey(rand.Reader, rsaBits) // can't use constant reader here
	notBefore := time.Now()
	notAfter := notBefore.Add(30 * time.Second)
	serialNumberLimit := new(big.Int).Lsh(big.NewInt(1), 128)
	serialNumber, _ := rand.Int(cryptoRand, serialNumberLimit)
	template := x509.Certificate{
		SerialNumber: serialNumber,
		Subject: pkix.Name{
			Organization: []string{"Dummy cert for testing"},
		},
		NotBefore: notBefore,
		NotAfter:  notAfter,

		KeyUsage:              x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
		BasicConstraintsValid: true,

		IPAddresses: []net.IP{net.IPv4(128, 66, 0, 1)}, // RFC5737 documentation block
		DNSNames:    []string{"example.net"},

		IsCA: false, // not passing KeyUsageCertSign
	}
	derBytes, _ := x509.CreateCertificate(cryptoRand, &template, &template, publicKey(privateKey), privateKey)
	pemCert := pem.EncodeToMemory(&pem.Block{Type: "CERTIFICATE", Bytes: derBytes})
	pemKey := pem.EncodeToMemory(pemBlockForKey(privateKey))
	cert, _ := tls.X509KeyPair(pemCert, pemKey)
	return cert
}
