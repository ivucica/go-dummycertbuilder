package main

import (
	"log"
	"net"
	"badc0de.net/pkg/dummycertbuilder"
	"crypto/tls"
)

	
func main() {
	log.Print("building cert...")
	cert := dummycertbuilder.GetDummyCert()
	log.Print("cert built")
	ln, _ := net.Listen("tcp", ":9912")
	for {
		conn, _ := ln.Accept()
		log.Print("accepted")
		tlscon := tls.Server(conn, &tls.Config{
			Certificates: []tls.Certificate{cert},
		})
		tlscon.Write([]byte("hello\n"))
		tlscon.Close()
	}

}
